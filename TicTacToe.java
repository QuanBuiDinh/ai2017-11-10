/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tictactoe;

import java.util.Random;

/**
 *
 * @author Asus
 */
public class TicTacToe {
    public static void main(String[] args) {
        Board b = new Board();
        Random rand = new Random();
        
        System.out.println("----------------Tic Tac Toe Game -------------------");
        System.out.println("Luật chơi: ");
        System.out.println("Đánh vào các vị trí trong bảng sao cho tạo thành 1 đường thẳng hoặc 1 đường chéo");
        System.out.println("Các vị trí trong bảng: ");
        System.out.println("[0][0] | [0][1] | [0][2]");
        System.out.println("-------------------------");
        System.out.println("[1][0] | [1][1] | [1][2]");
        System.out.println("-------------------------");
        System.out.println("[2][0] | [2][1] | [2][2]");
        b.displayBoard();

        System.out.println("Chọn lượt đi đầu tiên: (1)Máy tính (2)Bạn: ");
        int choice = b.scan.nextInt();
        if (choice == 1) {
            Point p = new Point(rand.nextInt(3), rand.nextInt(3));
            b.placeAMove(p, 1);
            b.displayBoard();
        }

        while (!b.isGameOver()) {
            System.out.println("Lượt của bạn: ");
            Point userMove = new Point(b.scan.nextInt(), b.scan.nextInt());

            b.placeAMove(userMove, 2);
            b.displayBoard();
            if (b.isGameOver()) break;
            
            b.alphaBetaMinimax(Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 1);
            for (PointsAndScores pas : b.rootsScore) 
                System.out.println("Point: " + pas.point + " Score: " + pas.score);
            
            b.placeAMove(b.returnBestMove(), 1);
            b.displayBoard();
        }
        if (b.hasXWon()) {
            System.out.println("You lost!");
        } else if (b.hasOWon()) {
            System.out.println("You win!");
        } else {
            System.out.println("Draw!");
        }
    }
    
}
